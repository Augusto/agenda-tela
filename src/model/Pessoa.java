package model;

public class Pessoa{
	private String nome;
	private String telefone;
	private String idade;
	private String cpf;

	public Pessoa(){
	}
	
	public Pessoa(String nome){
		this.nome = nome;
	}

	public Pessoa( String nome, String cpf){
		this.nome = nome;
		this.cpf = cpf;
	}

	public void setNome( String nome ){
		this.nome = nome;
	}

	public String getNome( ){
		return this.nome;
	}
	
	public void setTelefone( String telefone ){
		this.telefone = telefone;
	}

	public String getTelefone(){
		return this.telefone;
	}	

	public void setIdade( String idade){
		this.idade = idade;
	}

	public String getIdade(){
		return this.idade;
	}

	public void setCpf( String cpf){
		this.cpf = cpf;
	}

	public String getCpf(){
		return this.cpf;
	}

}
