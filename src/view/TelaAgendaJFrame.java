/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view;
import controller.ControllerPessoa;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Pessoa;

/**
 *
 * @author ramsesaugusto
 */
public class TelaAgendaJFrame extends javax.swing.JFrame {
    private ControllerPessoa umControlePessoa;
    private Pessoa umaPessoa;
    private boolean novoRegistro;
    /**
     * Creates new form TelaAgendaJFrame
     */
    public TelaAgendaJFrame() {
        initComponents();
        this.umControlePessoa = new ControllerPessoa();
    }

    public void preencherCampos() {
        jTextFieldNome.setText(umaPessoa.getNome());
        jTextFieldCpf.setText(umaPessoa.getCpf());
        jTextFieldTelefone.setText(umaPessoa.getTelefone());
        jTextFieldIdade.setText(umaPessoa.getIdade());
    }
    
    private void pesquisarUmaPessoaTabela(String nome) {
        Pessoa umaPessoaPesquisada = umControlePessoa.localizar(nome);

        if (umaPessoaPesquisada == null) {
            exibirInformacao("Pessoa não encontrada");
        } else {
            this.umaPessoa = umaPessoaPesquisada;
        }
    }
    
    private void exibirInformacao(String info) {
        JOptionPane.showMessageDialog(this, info, "Atenção", JOptionPane.INFORMATION_MESSAGE);
    }
    
    public void limparCampos(){
        jTextFieldNome.setText(null);
        jTextFieldCpf.setText(null);
        jTextFieldIdade.setText(null);
        jTextFieldTelefone.setText(null);
    }
    
    public void salvarPessoa() {
        if(novoRegistro == true){
            this.umaPessoa = new Pessoa();
        }
        else{
            
        }
        umaPessoa.setNome(jTextFieldNome.getText());
        umaPessoa.setIdade(jTextFieldIdade.getText());
        umaPessoa.setCpf(jTextFieldCpf.getText());
        umaPessoa.setTelefone(jTextFieldTelefone.getText());
        
        if (novoRegistro == true) {
            umControlePessoa.adicionar(umaPessoa);
            this.exibirInformacao("Cadastro efetuado com sucesso.");
        }
        else{
            this.exibirInformacao("Alteração efetuada com sucesso.");
        }
        novoRegistro = true;        
        this.limparCampos();
        this.carregarListaPessoas();
                
    }
    
    public void carregarListaPessoas(){
        ArrayList<Pessoa> listaPessoas = umControlePessoa.getListaPessoas();
        DefaultTableModel model = (DefaultTableModel) jTablePessoas.getModel();
        model.setRowCount(0);
        for (Pessoa novaPessoa : listaPessoas) {
            model.addRow(new String[]{novaPessoa.getNome(), novaPessoa.getCpf(), novaPessoa.getTelefone(), novaPessoa.getIdade()});
        }
        jTablePessoas.setModel(model);
    }
    
    private void pesquisarUmaPessoa(String nome) {
        Pessoa umaPessoaPesquisada = umControlePessoa.localizar(nome);

        if (umaPessoaPesquisada == null) {
            this.carregarListaPessoas();
            exibirInformacao("Pessoa não encontrada.");
        } else {
            this.umaPessoa = umaPessoaPesquisada;
            DefaultTableModel model = (DefaultTableModel) jTablePessoas.getModel();
            model.setRowCount(0);
            model.addRow(new String[]{umaPessoaPesquisada.getNome(), umaPessoaPesquisada.getCpf(), umaPessoaPesquisada.getTelefone(), umaPessoaPesquisada.getIdade()});
            jTablePessoas.setModel(model);
        }
        jButtonEditar.setEnabled(false);
        jButtonExcluir.setEnabled(false);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPaneMenu = new javax.swing.JTabbedPane();
        jPanelMenu = new javax.swing.JPanel();
        jLabelAgendaContatos = new javax.swing.JLabel();
        jButtonNovo = new javax.swing.JButton();
        jButtonEditar = new javax.swing.JButton();
        jButtonExcluir = new javax.swing.JButton();
        jTextFieldPesquisar = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTablePessoas = new javax.swing.JTable();
        jButtonPesquisar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabelNome = new javax.swing.JLabel();
        jTextFieldNome = new javax.swing.JTextField();
        jTextFieldCpf = new javax.swing.JTextField();
        jLabelCpf = new javax.swing.JLabel();
        jLabelNovoContato = new javax.swing.JLabel();
        jLabelIdade = new javax.swing.JLabel();
        jTextFieldIdade = new javax.swing.JTextField();
        jLabelTelefone = new javax.swing.JLabel();
        jTextFieldTelefone = new javax.swing.JTextField();
        jButtonSalvar = new javax.swing.JButton();
        jButtonResetar = new javax.swing.JButton();
        jButtonMenu = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTabbedPaneMenu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTabbedPaneMenuMouseClicked(evt);
            }
        });

        jLabelAgendaContatos.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabelAgendaContatos.setText("Agenda de Contatos");

        jButtonNovo.setText("Novo");
        jButtonNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNovoActionPerformed(evt);
            }
        });

        jButtonEditar.setText("Editar");
        jButtonEditar.setEnabled(false);
        jButtonEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditarActionPerformed(evt);
            }
        });

        jButtonExcluir.setText("Excluir");
        jButtonExcluir.setEnabled(false);
        jButtonExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonExcluirActionPerformed(evt);
            }
        });

        jTextFieldPesquisar.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                jTextFieldPesquisarInputMethodTextChanged(evt);
            }
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
        });
        jTextFieldPesquisar.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jTextFieldPesquisarPropertyChange(evt);
            }
        });

        jTablePessoas.setModel(new javax.swing.table.DefaultTableModel 
            (
                null,
                new String [] {
                    "Nome", "CPF", "Telefone", "Idade"
                }
            )
            {
                @Override    
                public boolean isCellEditable(int rowIndex, int mColIndex) {
                    return false;
                }
            });
            jTablePessoas.setColumnSelectionAllowed(true);
            jTablePessoas.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jTablePessoasMouseClicked(evt);
                }
            });
            jScrollPane1.setViewportView(jTablePessoas);
            jTablePessoas.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

            jButtonPesquisar.setText("Pesquisar");
            jButtonPesquisar.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonPesquisarActionPerformed(evt);
                }
            });

            javax.swing.GroupLayout jPanelMenuLayout = new javax.swing.GroupLayout(jPanelMenu);
            jPanelMenu.setLayout(jPanelMenuLayout);
            jPanelMenuLayout.setHorizontalGroup(
                jPanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelMenuLayout.createSequentialGroup()
                    .addGroup(jPanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelMenuLayout.createSequentialGroup()
                            .addGroup(jPanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanelMenuLayout.createSequentialGroup()
                                    .addGap(109, 109, 109)
                                    .addComponent(jButtonNovo)
                                    .addGap(18, 18, 18)
                                    .addComponent(jButtonEditar)
                                    .addGap(18, 18, 18)
                                    .addComponent(jButtonExcluir))
                                .addGroup(jPanelMenuLayout.createSequentialGroup()
                                    .addGap(108, 108, 108)
                                    .addComponent(jLabelAgendaContatos)))
                            .addGap(0, 12, Short.MAX_VALUE))
                        .addGroup(jPanelMenuLayout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(jPanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelMenuLayout.createSequentialGroup()
                                    .addGap(0, 0, Short.MAX_VALUE)
                                    .addComponent(jTextFieldPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(4, 4, 4)
                                    .addComponent(jButtonPesquisar)))))
                    .addContainerGap())
            );
            jPanelMenuLayout.setVerticalGroup(
                jPanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelMenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(jLabelAgendaContatos)
                    .addGap(18, 18, 18)
                    .addGroup(jPanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButtonPesquisar))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButtonNovo)
                        .addComponent(jButtonEditar)
                        .addComponent(jButtonExcluir)))
            );

            jTabbedPaneMenu.addTab("Menu", jPanelMenu);

            jLabelNome.setText("Nome:");

            jLabelCpf.setText("Cpf:");

            jLabelNovoContato.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
            jLabelNovoContato.setText("Novo Contato");

            jLabelIdade.setText("Idade:");

            jLabelTelefone.setText("Telefone:");

            jButtonSalvar.setText("Salvar");
            jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonSalvarActionPerformed(evt);
                }
            });

            jButtonResetar.setText("Resetar");
            jButtonResetar.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonResetarActionPerformed(evt);
                }
            });

            jButtonMenu.setText("Menu");
            jButtonMenu.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonMenuActionPerformed(evt);
                }
            });

            javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
            jPanel1.setLayout(jPanel1Layout);
            jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(132, 132, 132)
                            .addComponent(jLabelNovoContato))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(22, 22, 22)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelNome)
                                        .addComponent(jLabelCpf)
                                        .addComponent(jLabelIdade))
                                    .addGap(27, 27, 27)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(jTextFieldIdade, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                                            .addComponent(jTextFieldCpf, javax.swing.GroupLayout.Alignment.LEADING))))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabelTelefone)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(jTextFieldTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 94, Short.MAX_VALUE))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(jButtonSalvar)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jButtonResetar)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                    .addComponent(jButtonMenu)))))
                    .addGap(26, 26, 26))
            );
            jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(14, 14, 14)
                    .addComponent(jLabelNovoContato)
                    .addGap(30, 30, 30)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelNome)
                        .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabelCpf))
                    .addGap(18, 18, 18)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelIdade)
                        .addComponent(jTextFieldIdade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelTelefone)
                        .addComponent(jTextFieldTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(27, 27, 27)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButtonSalvar)
                        .addComponent(jButtonMenu)
                        .addComponent(jButtonResetar))
                    .addContainerGap(30, Short.MAX_VALUE))
            );

            jTabbedPaneMenu.addTab("Novo Contato", jPanel1);

            javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
            getContentPane().setLayout(layout);
            layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jTabbedPaneMenu)
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jTabbedPaneMenu)
            );

            pack();
        }// </editor-fold>//GEN-END:initComponents

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        // TODO add your handling code here:
        this.salvarPessoa();
        this.carregarListaPessoas();
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jButtonNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNovoActionPerformed
        // TODO add your handling code here:
        this.novoRegistro = true;
        jTabbedPaneMenu.setSelectedComponent(jPanel1);
    }//GEN-LAST:event_jButtonNovoActionPerformed

    private void jButtonEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditarActionPerformed
        // TODO add your handling code here:
        jTabbedPaneMenu.setSelectedComponent(jPanel1);
        this.novoRegistro = false;
        jButtonEditar.setEnabled(false);
        jButtonExcluir.setEnabled(false);
        this.preencherCampos();
    }//GEN-LAST:event_jButtonEditarActionPerformed

    private void jButtonExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluirActionPerformed
        // TODO add your handling code here:
        jButtonEditar.setEnabled(false);
        jButtonExcluir.setEnabled(false);
        umControlePessoa.excluir(umaPessoa);
        umaPessoa = null;
        this.limparCampos();
        this.carregarListaPessoas();
    }//GEN-LAST:event_jButtonExcluirActionPerformed

    private void jButtonResetarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonResetarActionPerformed
        // TODO add your handling code here:
        this.limparCampos();
    }//GEN-LAST:event_jButtonResetarActionPerformed

    private void jButtonMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonMenuActionPerformed
        // TODO add your handling code here:
        jTabbedPaneMenu.setSelectedComponent(jPanelMenu);
    }//GEN-LAST:event_jButtonMenuActionPerformed

    private void jTablePessoasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTablePessoasMouseClicked
        // TODO add your handling code here:
        if (jTablePessoas.isEnabled()) {
            jButtonEditar.setEnabled(true);
            jButtonExcluir.setEnabled(true);
            DefaultTableModel model = (DefaultTableModel) jTablePessoas.getModel();
            String nome = (String) model.getValueAt(jTablePessoas.getSelectedRow(), 0);
            this.pesquisarUmaPessoaTabela(nome);
        }
    }//GEN-LAST:event_jTablePessoasMouseClicked

    private void jTabbedPaneMenuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPaneMenuMouseClicked
        // TODO add your handling code here:
        this.novoRegistro = true;
    }//GEN-LAST:event_jTabbedPaneMenuMouseClicked

    private void jTextFieldPesquisarPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jTextFieldPesquisarPropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldPesquisarPropertyChange

    private void jTextFieldPesquisarInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_jTextFieldPesquisarInputMethodTextChanged
        // TODO add your handling code here:
        
    }//GEN-LAST:event_jTextFieldPesquisarInputMethodTextChanged

    private void jButtonPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPesquisarActionPerformed
        // TODO add your handling code here:
                
        String pesquisa = jTextFieldPesquisar.getText();
        jTextFieldPesquisar.setText(null);
        if (pesquisa != null) {
            this.pesquisarUmaPessoa(pesquisa);
        }
        
    }//GEN-LAST:event_jButtonPesquisarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaAgendaJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaAgendaJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaAgendaJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaAgendaJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaAgendaJFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonEditar;
    private javax.swing.JButton jButtonExcluir;
    private javax.swing.JButton jButtonMenu;
    private javax.swing.JButton jButtonNovo;
    private javax.swing.JButton jButtonPesquisar;
    private javax.swing.JButton jButtonResetar;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JLabel jLabelAgendaContatos;
    private javax.swing.JLabel jLabelCpf;
    private javax.swing.JLabel jLabelIdade;
    private javax.swing.JLabel jLabelNome;
    private javax.swing.JLabel jLabelNovoContato;
    private javax.swing.JLabel jLabelTelefone;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelMenu;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPaneMenu;
    private javax.swing.JTable jTablePessoas;
    private javax.swing.JTextField jTextFieldCpf;
    private javax.swing.JTextField jTextFieldIdade;
    private javax.swing.JTextField jTextFieldNome;
    private javax.swing.JTextField jTextFieldPesquisar;
    private javax.swing.JTextField jTextFieldTelefone;
    // End of variables declaration//GEN-END:variables
}
