package teste;
import controller.*;
import model.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ControllerPessoaTeste {
	ControllerPessoa umControlePessoa;
	Pessoa novaPessoa;
	
	@Before
	public void setUp() throws Exception {
		umControlePessoa = new ControllerPessoa();
	}

	@Test
	public void adicionarTest() {
		novaPessoa = new Pessoa("Lucas");
		umControlePessoa.adicionar(novaPessoa);
		assertEquals(novaPessoa, umControlePessoa.localizar("Lucas"));
	}
	
	@Test
	public void excluirTest(){
		novaPessoa = new Pessoa("Maria");
		umControlePessoa.adicionar(novaPessoa);
		umControlePessoa.excluir(novaPessoa);
		assertNull(umControlePessoa.localizar("Maria"));
	}
	
	@Test
	public void localizarTest(){
		novaPessoa = new Pessoa("Joao");
		umControlePessoa.adicionar(novaPessoa);
		assertNotNull(umControlePessoa.localizar("Joao"));
	}
	
	@Test
	public void getListaPessoasTest(){
		novaPessoa = new Pessoa("Maria");
		umControlePessoa.adicionar(novaPessoa);
		assertNotNull(umControlePessoa.getListaPessoas());
	}

}
